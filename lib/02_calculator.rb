def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(x)
  x.empty? ? 0 : x.inject(&:+)
end

def multiply(*numbers)
  numbers.inject(&:*)
end

def power(x, y)
  x ** y
end

def factorial(x)
  if n == 0
    1
  else
    n * factorial(n-1)
  end
end 
