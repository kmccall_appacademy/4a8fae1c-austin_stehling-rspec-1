def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, x = nil)
  x.nil? ? ((string << " ") * 2).strip : ((string << " ") * x).strip
end

def start_of_word(string, x)
  string[0...x]
end

def first_word(string)
  string.split(" ")[0]
end

def titleize(title)
  title = title.capitalize
  title_one = []
  words = ["and", "over", "the"]
  title.split.each{|x|
    if words.include?(x)
      title_one << x
    else
      title_one << x.capitalize
    end
  }
  title_one.join(" ")
end 
