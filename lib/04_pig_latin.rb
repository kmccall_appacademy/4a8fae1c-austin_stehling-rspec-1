def translate(words)
  arr = []
  vowels = %w(a e i o u)
  words.split(" ").each{|x|
    if vowels.include?(x[0])
      arr << (x << "ay")
    else
      i = 0
      until vowels.include?(x[i])
        i += 1
      end
      x = (x[i..-1] << x[0...i])
      if x[-1] == "q"
        x = (x[1..-1] << x[0] << "ay")
      else
        x = (x << "ay")
      end
      arr << x
    end
  }
  arr.join(" ")
end 
